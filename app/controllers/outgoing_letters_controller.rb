class OutgoingLettersController < ApplicationController
  before_action :set_outgoing_letter, only: [:show, :edit, :update, :destroy]

  # GET /outgoing_letters
  # GET /outgoing_letters.json
  def index
    @outgoing_letters = OutgoingLetter.all
  end

  # GET /outgoing_letters/1
  # GET /outgoing_letters/1.json
  def show
  end

  # GET /outgoing_letters/new
  def new
    @outgoing_letter = OutgoingLetter.new
  end

  # GET /outgoing_letters/1/edit
  def edit
  end

  # POST /outgoing_letters
  # POST /outgoing_letters.json
  def create
    @outgoing_letter = OutgoingLetter.new(outgoing_letter_params)

    respond_to do |format|
      if @outgoing_letter.save
        format.html { redirect_to @outgoing_letter, notice: 'Outgoing letter was successfully created.' }
        format.json { render :show, status: :created, location: @outgoing_letter }
      else
        format.html { render :new }
        format.json { render json: @outgoing_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /outgoing_letters/1
  # PATCH/PUT /outgoing_letters/1.json
  def update
    respond_to do |format|
      if @outgoing_letter.update(outgoing_letter_params)
        format.html { redirect_to @outgoing_letter, notice: 'Outgoing letter was successfully updated.' }
        format.json { render :show, status: :ok, location: @outgoing_letter }
      else
        format.html { render :edit }
        format.json { render json: @outgoing_letter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /outgoing_letters/1
  # DELETE /outgoing_letters/1.json
  def destroy
    @outgoing_letter.destroy
    respond_to do |format|
      format.html { redirect_to outgoing_letters_url, notice: 'Outgoing letter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_outgoing_letter
      @outgoing_letter = OutgoingLetter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def outgoing_letter_params
      params.require(:outgoing_letter).permit(:title, :description)
    end
end
