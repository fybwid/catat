class DashboardController < ApplicationController
  def index
    @all_outgoing_letter = OutgoingLetter.count
  end
  
  def dashboard_params
    params.require(:outgoing_letter).permit(:title, :description)
  end
end
