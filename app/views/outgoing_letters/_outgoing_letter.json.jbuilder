json.extract! outgoing_letter, :id, :title, :description, :created_at, :updated_at
json.url outgoing_letter_url(outgoing_letter, format: :json)
