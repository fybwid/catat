require "test_helper"

# To be handled correctly this spec must end with "Integration Test"
describe "WelcomeDashboardFlow Integration Test" do
  # it "must be a real test" do
  #   flunk "Need real tests"
  # end
  test "can flow from Welcome to Dashboard page" do
    get "/"
    assert_response :success
    puts "✔ can go to Welcome page"
    # Should check Welcome page consist of what (button, text, etc)
    get "/dashboard/index"
    assert_response :success
    puts "✔ can go to Dashboard - index page"
    # Should check Welcome page consist of what (button, text, etc)
  end
end
