require "test_helper"

# To be handled correctly this spec must end with "Integration Test"
describe "OutgoingLetterFlow Integration Test" do
  # it "must be a real test" do
  #   flunk "Need real tests"
  # end
  
  # called before every single test
  setup do
    @outgoing_letter = outgoing_letters(:one)
  end
  
  # called after every single test
  teardown do
    # when controller is using cache it may be a good idea to reset it afterwards
    Rails.cache.clear
  end
  
  test "should create outgoing letter" do
    get "/outgoing_letters/new"
    assert_response :success
   
    post "/outgoing_letters",
      params: { outgoing_letter: { title: "can create outgoing letter", description: "created outgoing letter successfully." } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    #assert_select "p", "Title:\n  Outgoing letter was successfully created."
    assert_select "p", "Outgoing letter was successfully created."
  end
  
  test "should show outgoing letter" do
    get outgoing_letter_url(@outgoing_letter)
    assert_response :success
  end
  
  
  test "should update outgoing letter" do
    patch outgoing_letter_url(@outgoing_letter), params: { outgoing_letter: { title: "can update outgoing letter", description: "outgoing letter successfully updated." } }
   
    assert_redirected_to outgoing_letter_path(@outgoing_letter)
    # Reload association to fetch updated data and assert that title is updated.
    @outgoing_letter.reload
    assert_equal "can update outgoing letter", @outgoing_letter.title
    assert_equal "outgoing letter successfully updated.", @outgoing_letter.description
  end
  
  test "should destroy outgoing letter" do
    assert_difference('OutgoingLetter.count', -1) do
      delete outgoing_letter_url(@outgoing_letter)
    end
   
    assert_redirected_to outgoing_letters_path
  end
end