require "test_helper"

describe DashboardController do
  it "should get index" do
    get dashboard_index_url
    value(response).must_be :success?
  end
  it "can see Dashboard page details" do
    get "/dashboard/index"
    assert_select "title", {:count=>1, :text=>"Catat"},
      "Wrong title or more than one title element"
    assert_select "h1", {:count=>1, :text=>"Dashboard"},
      "Wrong h1 or more than one h1 element"
    assert_select 'a[href="/outgoing_letters"]', {:count=>1, :text=>"Outgoing Letter"},
      "Wrong, no <a> element with text equal Outgoing Letter"
    #Test menu with a href
    #assert_select '.menu a[href=?]', 'http://test.host/dashboard',
    #  { :count => 1, :text => 'Your Dashboard' }
    #assert_select '.menu a[href=?]', /\Ahttp:\/\/test.host\/dashboard\z/,
    #  { :count => 1, :text => 'Your Dashboard' }
  end

  # it "must be a real test" do
  #   flunk "Need real tests"
  # end
end
