require "test_helper"

describe WelcomeController do
  it "should get index" do
    get welcome_index_url
    value(response).must_be :success?
  end
  it "can see welcome page details" do
    get "/"
    assert_select "h1", {:count=>1, :text=>"Welcome to Catat"},
      "Wrong <h1> text"
    assert_select "p", {:count=>1, :text=>"An app used for optimizing documentations"},
      "Wrong <p> text"
    assert_select 'a[href="/dashboard/index"]', {:count=>1, :text=>"Dashboard"},
      "Wrong, no <a> element with text equal Dashboard"
    assert_select 'a[href="/signin"]', {:count=>1, :text=>"Sign in"},
      "Wrong, no <a> element with text equal Sign in"
    assert_select 'a[href="/signup"]', {:count=>1, :text=>"Sign up"},
      "Wrong, no <a> element with text equal Sign up"
  end
end