require "test_helper"

describe OutgoingLettersController do
  let(:outgoing_letter) { outgoing_letters :one }

  it "gets index" do
    get outgoing_letters_url
    value(response).must_be :success?
  end

  it "gets new" do
    get new_outgoing_letter_url
    value(response).must_be :success?
  end

  it "creates outgoing_letter" do
    expect {
      post outgoing_letters_url, params: { outgoing_letter: { description: outgoing_letter.description, title: outgoing_letter.title } }
    }.must_change "OutgoingLetter.count"

    must_redirect_to outgoing_letter_path(OutgoingLetter.last)
  end

  it "shows outgoing_letter" do
    get outgoing_letter_url(outgoing_letter)
    value(response).must_be :success?
  end

  it "gets edit" do
    get edit_outgoing_letter_url(outgoing_letter)
    value(response).must_be :success?
  end

  it "updates outgoing_letter" do
    patch outgoing_letter_url(outgoing_letter), params: { outgoing_letter: { description: outgoing_letter.description, title: outgoing_letter.title } }
    must_redirect_to outgoing_letter_path(outgoing_letter)
  end

  it "destroys outgoing_letter" do
    expect {
      delete outgoing_letter_url(outgoing_letter)
    }.must_change "OutgoingLetter.count", -1

    must_redirect_to outgoing_letters_path
  end
end
