require "test_helper"

describe OutgoingLetter do
  let(:outgoing_letter) { OutgoingLetter.new }

  it "must be valid" do
    value(outgoing_letter).must_be :valid?
  end
end
