# Catat

## Requirements
* Ruby version: 2.4.2
* Rails version: 5.4.1
* Postgresql version: 9.3.18

## Deployment
### Heroku
```bash
heroku version   # Check Heroku version
heroku login     # Login to Heroku
heroku keys:add  # Add id_rsa.pub to Heroku
heroku create     # Create new Heroku app container
```

## Test
### [Minitest](https://github.com/blowmage/minitest-rails)
Add Minitest to Gemfile
```bash
gem "minitest-rails"
```

Next run the installation generator
```bash
rails generate minitest:install
```

> This will add the `test_helper.rb` file to the `test` directory

#### Integration Test
Command:
```bash
rails generate integration_test welcome_dashboard_flow
```
Result:
```bash
Running via Spring preloader in process 12545
      invoke  minitest
      create    test/integration/welcome_dashboard_flow_test.rb
```
> Used to test workflow for user visit from welcome to dashboard

More [testing documentations](http://guides.rubyonrails.org/testing.html#test-helpers)

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
