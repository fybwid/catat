Rails.application.routes.draw do
  resources :outgoing_letters
  get 'dashboard/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'welcome/index'
  
  root 'welcome#index'
end
